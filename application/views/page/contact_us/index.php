<div class="row">
  <div class="col-12 mb-4">
    <h1 class="text-center">KONTAK KAMI</h1>
  </div>
  <div class="col-lg-6 col-md-6 col-12 mb-4">
    <div class="row mt-2">
      <div class="col-1">
        <i class="fas fa-map-marker-alt"></i>
      </div>
      <div class="col">
        <span class="contact-address">Jalan Lapangan Banteng Barat No. 3-4, Jakarta 10710</span>
      </div>
    </div>
    <div class="row mt-2">
      <div class="col-1">
        <i class="fas fa-phone"></i>
      </div>
      <div class="col">
        <span class="contact-phone">021 123 456 11</span>
      </div>
    </div>
    <div class="row mt-2">
      <div class="col-1">
        <i class="fas fa-envelope"></i>
      </div>
      <div class="col">
        <span class="contact-email">admin@mail.com</span>
      </div>
    </div>
    <div class="row mt-4">
      <div class="col-12 contact-map">
        <div class="mapouter"><div class="gmap_canvas"><iframe width="100%" height="200" id="gmap_canvas" src="https://maps.google.com/maps?q=monas&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://123movies-a.com"></a><br><style>.mapouter{position:relative;text-align:right;height:200px;width:100%;}</style><a href="https://www.embedgooglemap.net"></a><style>.gmap_canvas {overflow:hidden;background:none!important;height:200px;width:100%;}</style></div></div>
      </div>
    </div>
  </div>
  <div class="col-lg-6 col-md-6 col-12 mb-4">
    <div class="card">
      <div class="card-body">
        <form>
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Nama</label>
            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
          </div>
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Email</label>
            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
          </div>
          <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Pesan</label>
            <textarea class="form-control" id="exampleInputPassword1"></textarea>
          </div>
          <div class="mb-3 text-end">
            <button type="submit" class="btn btn-success">Kirim Pesan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>