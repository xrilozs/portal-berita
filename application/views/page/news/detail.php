<div class="row mb-2" id="news-image-section">
  <div class="col-12">
    <img src="https://placehold.co/600x400" id="news-image" style="width:100%;height:500px; object-fit: cover" class="img-fluid">
  </div>
</div>
<div class="row" id="news-nav-section">
  <div class="col-6 col-md-7 col-sm-7">
    <i class="fas fa-calendar-alt"></i> <span id="news-date"></span>
  </div>
  <div class="col-6 col-md-5 col-sm-5 d-flex justify-content-end">
    <nav aria-label="breadcrumb" style="--bs-breadcrumb-divider: '>';">
      <ol class="breadcrumb">
        <li class="breadcrumb-item" aria-current="page">
          Berita
        </li>
        <li class="breadcrumb-item" aria-current="page" id="news-category-text">
          <a href="<?=base_url('berita/').$news_category_alias;?>"><?=$news_category;?></a>
        </li>
      </ol>
    </nav>
  </div>
</div>
<div class="row mb-4" id="news-content-section">
  <div class="col-12" style="text-align:justify;">
    <h1 id="news-title"></h1>
    <div id="news-content">
      <?=$loading;?>
    </div>
  </div>
</div>

<?=$news_related;?>