<h3 id="news-category"><?=$news_category;?></h3>

<?=$news_slider;?>

<div class="row my-4" id="list-news">
  <?=$loading;?>
</div>

<nav aria-label="Page navigation">
  <ul class="pagination justify-content-center" id="news-pagination">
  </ul>
</nav>