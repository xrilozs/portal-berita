<div class="row" id="video-nav-section">
  <div class="col-6">
    <i class="fas fa-calendar-alt"></i> <span id="video-date"></span>
  </div>
  <div class="col-6 d-flex justify-content-end">
    <nav aria-label="breadcrumb" style="--bs-breadcrumb-divider: '>';">
      <ol class="breadcrumb">
        <li class="breadcrumb-item" aria-current="page">
          Galeri
        </li>
        <li class="breadcrumb-item" aria-current="page">
          <a href="<?=base_url('galeri/video');?>">Video</a>
        </li>
      </ol>
    </nav>
  </div>
</div>
<div class="row">
  <div class="col-12">
    <h1 id="video-title"></h1>
  </div>
</div>
<div class="row mb-4" id="video-content">
  <div class="col-12 video-container">
    <iframe src="#" id="video-player" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
  </div>
</div>
<div class="row">
  <div class="col-12">
    <p align="justify" id="video-description">
    </p>
  </div>
</div>
