<div class="row" id="photo-nav-section">
  <div class="col-6">
    <i class="fas fa-calendar-alt"></i> <span id="photo-date"></span>
  </div>
  <div class="col-6 d-flex justify-content-end">
    <nav aria-label="breadcrumb" style="--bs-breadcrumb-divider: '>';">
      <ol class="breadcrumb">
        <li class="breadcrumb-item" aria-current="page">
          Galeri
        </li>
        <li class="breadcrumb-item" aria-current="page">
          <a href="<?=base_url('galeri/foto');?>">Foto</a>
        </li>
      </ol>
    </nav>
  </div>
</div>
<div class="row">
  <div class="col-12">
    <h1 id="photo-title"></h1>
  </div>
</div>
<div class="row mb-4" id="photo-content">
  <div class="slider slider-single" id="photo-display">
    <div><img src="https://placehold.co/600x400"></div>
  </div>
  <div class="col-12 d-flex justify-content-center my-4">
    <div class="slider slider-nav" style="width: 90%" id="photo-slider">
      <div class="mx-2"><img src="https://placehold.co/600x400"></div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-12">
    <p align="justify" id="photo-description">
    </p>
  </div>
</div>
