<div class="row mb-4" id="profile-nav-section">
  <div class="col-12 text-center">
    <h1>HASIL PERTANDINGAN</h1>
    <h5>MTQ/STQ PROVINSI KALTIM</h5>
  </div>
</div>
<div class="row mb-4 d-flex justify-content-center">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <div class="row d-flex justify-content-center">
          <div class="col-lg-3 col-md-3 col-12 my-1">
            <label class="fw-bold">Babak</label>
            <select class="form-select form-select-sm" id="babak-option">
              <option value="penyisihan">Penyisihan</option>
              <option value="final">Final</option>
            </select>
          </div>
          <div class="col-lg-3 col-md-3 col-12 my-1">
            <label class="fw-bold">Cabang</label>
            <select class="form-select form-select-sm" id="cabang-option">
            </select>
          </div>
          <div class="col-lg-3 col-md-3 col-12 my-1">
            <label class="fw-bold">Golongan</label>
            <select class="form-select form-select-sm" id="golongan-option" disabled>
              <option value="">--Pilih golongan--</option>
            </select>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row mt-5" id="init-section">
  <div class="col-12 d-flex justify-content-center fst-italic text-secondary">
    <p align="center" style="font-size:14px;">Silahkan pilih hasil pertandingan yang Anda inginkan</p>
  </div>
</div>

<div class="row mt-5" id="result-section" style="display:none;">
  <div class="col-12 table-responsive">
    <table class="table table-bordered table-hover" id="result-datatable">
      <thead>
        <tr>
          <th>No</th>
          <th>Foto</th>
          <th>Nama</th>
          <th>Nilai</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>1</td>
          <td class="d-flex justify-content-center align-items-center">
            <img src='https://avataaars.io/?avatarStyle=Circle&topType=NoHair&accessoriesType=Round&facialHairType=BeardMajestic&facialHairColor=BlondeGolden&clotheType=BlazerShirt&eyeType=Default&eyebrowType=AngryNatural&mouthType=Default&skinColor=Yellow' class="img-fluid" alt="foto peserta" loading="lazy" style="width:100px;"/>
          </td>
          <td>Peserta 1<br>(Kalimantan Timur)</td>
          <td class="text-center">
            100<br>
            <button class="btn btn-light btn-sm" data-bs-toggle="modal" data-bs-target="#result-detail-modal">
              <i class="fa fa-search"></i> Detail
            </button>
          </td>
        </tr>
        <tr>
          <td>2</td>
          <td class="d-flex justify-content-center align-items-center">
            <img src='https://avataaars.io/?avatarStyle=Circle&topType=NoHair&accessoriesType=Round&facialHairType=BeardMajestic&facialHairColor=BlondeGolden&clotheType=BlazerShirt&eyeType=Default&eyebrowType=AngryNatural&mouthType=Default&skinColor=Yellow' class="img-fluid" alt="foto peserta" loading="lazy" style="width:100px;"/>
          </td>
          <td>Peserta 2<br>(Kalimantan Timur)</td>
          <td class="text-center">
            100<br>
            <button class="btn btn-light btn-sm" data-bs-toggle="modal" data-bs-target="#result-detail-modal">
            <i class="fa fa-search"></i> Detail
            </button>
          </td>
        </tr>
        <tr>
          <td>3</td>
          <td class="d-flex justify-content-center align-items-center">
            <img src='https://avataaars.io/?avatarStyle=Circle&topType=NoHair&accessoriesType=Round&facialHairType=BeardMajestic&facialHairColor=BlondeGolden&clotheType=BlazerShirt&eyeType=Default&eyebrowType=AngryNatural&mouthType=Default&skinColor=Yellow' class="img-fluid" alt="foto peserta" loading="lazy" style="width:100px;"/>
          </td>
          <td>Peserta 3<br>(Kalimantan Timur)</td>
          <td class="text-center">
            100<br>
            <button class="btn btn-light btn-sm" data-bs-toggle="modal" data-bs-target="#result-detail-modal">
            <i class="fa fa-search"></i> Detail
            </button>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>

<div class="modal fade" id="result-detail-modal" tabindex="-1" aria-labelledby="result-detail-modal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-success text-white">
        <h5 class="modal-title" id="exampleModalLabel">Detail Nilai</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-4 col-md-3 col-12 text-center mb-4">
            <img src='https://avataaars.io/?avatarStyle=Circle&topType=NoHair&accessoriesType=Round&facialHairType=BeardMajestic&facialHairColor=BlondeGolden&clotheType=BlazerShirt&eyeType=Default&eyebrowType=AngryNatural&mouthType=Default&skinColor=Yellow' class="img-fluid" alt="foto peserta" loading="lazy" style="width:100px;"/><br>
            <span>Lorem Ipsum</span>
          </div>
          <div class="col-lg-8 col-md-9 col-12">
            <div class="row">
              <div class="col-6 my-1">
                <div class="card">
                  <div class="card-body text-center">
                    <h5>Penilaian 1</h5>
                    <span>25</span>
                  </div>
                </div>
              </div>
              <div class="col-6 my-1">
                <div class="card">
                  <div class="card-body text-center">
                    <h5>Penilaian 2</h5>
                    <span>25</span>
                  </div>
                </div>
              </div>
              <div class="col-6 my-1">
                <div class="card">
                  <div class="card-body text-center">
                    <h5>Penilaian 3</h5>
                    <span>25</span>
                  </div>
                </div>
              </div>
              <div class="col-6 my-1">
                <div class="card">
                  <div class="card-body text-center">
                    <h5>Penilaian 4</h5>
                    <span>25</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>