<div class="row mb-4" id="live-streaming-nav-section">
  <div class="col-12 text-center">
    <h1>LIVE STREAMING</h1>
    <h5>MTQ/STQ PROVINSI KALTIM</h5>
  </div>
</div>
<div class="row mb-4" id="live-streamin-focus">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-12 video-container">
            <iframe id="live-streaming-player" src="https://www.youtube.com/embed/jfKfPfyJRdk" id="video-player" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row d-flex justify-content-center mb-4">
  <section class="timeline-center">
    <?=$loading;?>
  </section>
</div>