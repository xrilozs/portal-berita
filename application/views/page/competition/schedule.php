<div class="row mb-4" id="profile-nav-section">
  <div class="col-12 text-center">
    <h1>JADWAL PERTANDINGAN</h1>
    <h5 id="event-name"></h5>
  </div>
</div>
<div class="row mb-4" id="schedule-pdf-section">
  <div class="col-12 mb-4 d-flex justify-content-center">
    <button class="btn btn-success" type="button" data-bs-toggle="collapse" data-bs-target="#pdf-section">
      <i class="fas fa-file-pdf"></i> Lihat Jadwal Dalam bentuk PDF
    </button>
  </div>
  <div id="pdf-section" class="col-12 collapse">
    <embed id="pdf" src="" width="100%" height="700px;">
    <div class="d-flex justify-content-center my-4">
      <button id="close-pdf" class="btn btn-danger" type="button" data-bs-toggle="collapse" data-bs-target="#pdf-section">
        Tutup PDF
      </button>
    </div>
  </div>
</div>
<div class="row d-flex justify-content-center mb-4">
  <section class="timeline-center" id="schedule-timeline">
  </section>
</div>
<div class="row mb-4 d-flex justify-content-center" >
  <div class="col-12" id="schedule-detail">
    <div class="card">
      <div class="card-body p-5">
        <div class="mb-4">
          <h3 class="card-title text-center" id="rundown-title"></h3>
        </div>
        <div id="content">
          <ul class="timeline" id="rundown-content">
            <?=$loading;?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>