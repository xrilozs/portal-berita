<div class="row" id="profile-nav-section">
  <div class="col-6">
    <i class="fas fa-calendar-alt"></i> <span id="profile-date"></span>
  </div>
  <div class="col-6 d-flex justify-content-end">
    <nav aria-label="breadcrumb" style="--bs-breadcrumb-divider: '>';">
      <ol class="breadcrumb">
        <li class="breadcrumb-item" aria-current="page">
          Profile
        </li>
        <li class="breadcrumb-item active" aria-current="page" id="profile-category-text">
          <?=$title;?>
        </li>
      </ol>
    </nav>
  </div>
</div>
<div class="row mb-4" id="profile-content-section">
  <div class="col-12" style="text-align:justify;">
    <h1 id="profile-title"><?=$title;?></h1>
    <div id="profile-content">
      <?=$loading;?>
    </div>
  </div>
</div>
