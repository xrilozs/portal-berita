<div class="row" id="list-document">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <?=$loading;?>
      </div>
    </div>
  </div>
</div>

<nav aria-label="Page navigation" class="mt-4">
  <ul class="pagination justify-content-center" id="document-pagination">
  </ul>
</nav>