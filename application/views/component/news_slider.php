<div id="slider-content" class="carousel carousel-dark slide" data-bs-ride="carousel">
  <div class="carousel-inner" id="slider-news">
    <div class="carousel-item active" data-bs-interval="10000">
      <?=$loading;?>
    </div>
  </div>
  <button class="carousel-control-prev" type="button" data-bs-target="#slider-content" data-bs-slide="prev">
    <i class="fas fa-chevron-circle-left fa-lg" style="color:#181c4e; font-size:30px;"></i>
    <span class="visually-hidden">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#slider-content" data-bs-slide="next">
    <i class="fas fa-chevron-circle-right fa-lg" style="color: #181c4e; font-size:30px;"></i>
    <span class="visually-hidden">Next</span>
  </button>
</div>