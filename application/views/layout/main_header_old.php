<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Portal Berita Islam</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <!-- <link id="favicon" href="" rel="icon"> -->

  <!-- Google Fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Sofia+Sans:wght@500&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.5.0-2/css/all.min.css" rel="stylesheet">
  <?= $title == 'galeri foto detail' ? '<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/foundation/5.5.0/css/foundation.css"/>' : ''; ?>
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="https://kenwheeler.github.io/slick/slick/slick-theme.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/slider.css');?>" rel="stylesheet" />
  <link href="<?=base_url('assets/css/main.css');?>" rel="stylesheet">
  <link href="<?=base_url('assets/vendor/image-hover/css/imagehover.min.css');?>" rel="stylesheet">
</head>

<body>
  <header>
    <div style="background-color:#1eb871">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-2">
            <div class="row m-2">
              <div class="col-4 col-lg-2 col-md-3 col-sm-4">
                <img src="<?=base_url('assets/img/logo.png');?>" alt="" height="100">
              </div>
              <div class="col-8 col-lg-10 col-md-9 col-sm-8 mt-2">
                <div class="row">
                  <div class="col-9 d-flex align-items-center">
                    <span class="p-2 text-white" style="font-size:24px; font-weight:bold;">Portal Berita<br>Islam Terkini</span>
                  </div>
                  <div class="col-3 d-flex align-items-center justify-content-end" id="navbar-offcanvas-section">
                    <button class="btn btn-success" type="button" data-bs-toggle="offcanvas" data-bs-target="#navbar-offcanvas" aria-controls="offcanvasRight">
                      <i class="fas fa-bars"></i>
                    </button>
                    <div class="offcanvas offcanvas-end" tabindex="-1" id="navbar-offcanvas" aria-labelledby="offcanvasNavbarLabel">
                      <div class="offcanvas-header">
                        <h5 class="offcanvas-title" id="offcanvasNavbarLabel">Menu</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                      </div>
                      <div class="offcanvas-body bg-success">
                        <ul class="navbar-nav justify-content-end flex-grow-1 pe-3">
                          <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="<?=base_url();?>">Beranda</a>
                          </li>
                          <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="berita-dropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                              Berita
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="berita-dropdown">
                              <li><a class="dropdown-item" href="<?=base_url('berita/nasional');?>">Nasional</a></li>
                              <li><a class="dropdown-item" href="<?=base_url('berita/daerah');?>">Daerah</a></li>
                            </ul>
                          </li>
                          <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="profil-dropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                              Profil
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="berita-dropdown">
                              <li><a class="dropdown-item" href="<?=base_url('profile/sejarah');?>">Sejarah</a></li>
                              <li><a class="dropdown-item" href="<?=base_url('profile/visi-misi');?>">Visi - Misi</a></li>
                              <li><a class="dropdown-item" href="<?=base_url('profile/struktur-organisasi');?>">Struktur Organisasi</a></li>
                            </ul>
                          </li>
                          <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="galeri-dropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                              Galeri
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="galeri-dropdown">
                              <li><a class="dropdown-item" href="<?=base_url('galeri/foto');?>">Foto</a></li>
                              <li><a class="dropdown-item" href="<?=base_url('galeri/video');?>">Video</a></li>
                            </ul>
                          </li>
                          <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="musabaqah-dropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                              Musabaqah
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="musabaqah-dropdown">
                              <li><a class="dropdown-item" href="#">MTQ</a></li>
                              <li><a class="dropdown-item" href="#">STQ</a></li>
                            </ul>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="<?=base_url('kontak-kami');?>">Kontak Kami</a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-2 d-flex align-items-center justify-content-center">
            <div id="banner-header" class="carousel slide " data-bs-ride="carousel">
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <img src="<?=base_url('assets/img/banner1.jpg');?>" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                  <img src="<?=base_url('assets/img/banner2.png');?>" class="d-block w-100" alt="...">
                </div>
              </div>
              <button class="carousel-control-prev" type="button" data-bs-target="#banner-header" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
              </button>
              <button class="carousel-control-next" type="button" data-bs-target="#banner-header" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>

    <nav class="navbar navbar-expand-md navbar-dark bg-success" id="navbar-header">
      <div class="container" style="font-weight:bold; color:white !important;">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse d-flex justify-content-center" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item px-2">
              <a class="nav-link" href="<?=base_url();?>">Beranda</a>
            </li>
            <li class="nav-item px-2 dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="berita-dropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Berita
              </a>
              <ul class="dropdown-menu" aria-labelledby="berita-dropdown">
                <li><a class="dropdown-item" href="<?=base_url('berita/nasional');?>">Nasional</a></li>
                <li><a class="dropdown-item" href="<?=base_url('berita/daerah');?>">Daerah</a></li>
              </ul>
            </li>
            <li class="nav-item px-2 dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="profil-dropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Profil
              </a>
              <ul class="dropdown-menu" aria-labelledby="berita-dropdown">
                <li><a class="dropdown-item" href="<?=base_url('profile/sejarah');?>">Sejarah</a></li>
                <li><a class="dropdown-item" href="<?=base_url('profile/visi-misi');?>">Visi - Misi</a></li>
                <li><a class="dropdown-item" href="<?=base_url('profile/struktur-organisasi');?>">Struktur Organisasi</a></li>
              </ul>
            </li>
            <li class="nav-item px-2 dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="galeri-dropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Galeri
              </a>
              <ul class="dropdown-menu" aria-labelledby="galeri-dropdown">
                <li><a class="dropdown-item" href="<?=base_url('galeri/foto');?>">Foto</a></li>
                <li><a class="dropdown-item" href="<?=base_url('galeri/video');?>">Video</a></li>
              </ul>
            </li>
            <li class="nav-item px-2 dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="musabaqah-dropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Musabaqah
              </a>
              <ul class="dropdown-menu" aria-labelledby="musabaqah-dropdown">
                <li><a class="dropdown-item" href="#">MTQ</a></li>
                <li><a class="dropdown-item" href="#">STQ</a></li>
              </ul>
            </li>
            <li class="nav-item px-2">
              <a class="nav-link" href="<?=base_url('kontak-kami');?>">Kontak Kami</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </header>