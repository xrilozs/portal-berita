<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title><?=ucwords($title);?> - Portal Berita Islam</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link id="favicon" href="" rel="icon">

  <!-- Google Fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Sofia+Sans:wght@500&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.5.0-2/css/all.min.css" rel="stylesheet">
  <?= $title == 'galeri foto detail' ? '<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/foundation/5.5.0/css/foundation.css"/>' : ''; ?>
  <!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css" rel="stylesheet" /> -->
  <!-- <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/> -->
  <!-- <link rel="stylesheet" type="text/css" href="https://kenwheeler.github.io/slick/slick/slick-theme.css" rel="stylesheet" /> -->
  <link rel="stylesheet" type="text/css" href="<?=base_url('assets/vendor/slick/slick/slick.css');?>" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<?=base_url('assets/vendor/slick/slick/slick-theme.css');?>" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/slider.css');?>" rel="stylesheet" />
  <link href="<?=base_url('assets/css/main.css');?>" rel="stylesheet">
  <link href="<?=base_url('assets/vendor/image-hover/css/imagehover.min.css');?>" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">

  <?= $title == 'jadwal perlombaan' ? '<link href="'.base_url('assets/css/timeline.css').'" rel="stylesheet">' : ""; ?>
</head>

<body>
  <header>
    <div style="background-color:#212529;">
      <div class="container">
        <div class="row">
          <div class="col-9 my-1 d-flex align-items-center">
            <div class="row">
              <div class="col-12">
                <a href="#" class="btn btn-sm btn-link text-white sm-facebook"><i class="fab fa-facebook-f"></i></a>
                <a href="#" class="btn btn-sm btn-link text-white sm-youtube"><i class="fab fa-youtube"></i></a>
                <a href="#" class="btn btn-sm btn-link text-white sm-twitter"><i class="fab fa-twitter"></i></a>
                <a href="#" class="btn btn-sm btn-link text-white sm-instagram"><i class="fab fa-instagram"></i></a>
                &nbsp;<span class="text-white">|</span>&nbsp;
                <a href="<?=base_url('kontak-kami')?>" class="text-white"><strong>Kontak Kami</strong></a>
              </div>
              <div class="col-12  text-white" id="header-date-left" style="display:none;">
              </div>
            </div>
          </div>
          <div class="col-3 my-1 d-flex justify-content-end align-items-center text-white">
            <div class="row">
              <div class="col-12" id="header-date-right">
              </div>
              <div class="col-12 d-flex align-items-center justify-content-end" id="navbar-offcanvas-section">
                <button class="btn btn-success" type="button" data-bs-toggle="offcanvas" data-bs-target="#navbar-offcanvas" aria-controls="offcanvasRight">
                  <i class="fas fa-bars"></i>
                </button>
                <div class="offcanvas offcanvas-end" tabindex="-1" id="navbar-offcanvas" aria-labelledby="offcanvasNavbarLabel">
                  <div class="offcanvas-header">
                    <h5 class="offcanvas-title" style="color:black;" id="offcanvasNavbarLabel">Menu</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                  </div>
                  <div class="offcanvas-body bg-success">
                    <ul class="navbar-nav justify-content-end flex-grow-1 pe-3">
                      <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="<?=base_url();?>">Beranda</a>
                      </li>
                      <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="berita-dropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                          Berita
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="berita-dropdown" id="category-news-menu-mobile">
                          <li><a class="dropdown-item" href="<?=base_url('berita/nasional');?>">Nasional</a></li>
                          <li><a class="dropdown-item" href="<?=base_url('berita/daerah');?>">Daerah</a></li>
                        </ul>
                      </li>
                      <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="profil-dropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                          Profil
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="profile-dropdown" id="profile-menu-mobile">
                          <li><a class="dropdown-item" href="<?=base_url('profile/sejarah');?>">Sejarah</a></li>
                          <li><a class="dropdown-item" href="<?=base_url('profile/visi-misi');?>">Visi - Misi</a></li>
                          <li><a class="dropdown-item" href="<?=base_url('profile/struktur-organisasi');?>">Struktur Organisasi</a></li>
                        </ul>
                      </li>
                      <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="galeri-dropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                          Galeri
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="galeri-dropdown">
                          <li><a class="dropdown-item" href="<?=base_url('galeri/foto');?>">Foto</a></li>
                          <li><a class="dropdown-item" href="<?=base_url('galeri/video');?>">Video</a></li>
                        </ul>
                      </li>
                      <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="musabaqah-dropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                          Musabaqah
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="musabaqah-dropdown">
                          <li><a class="dropdown-item" href="<?=base_url('jadwal');?>">Jadwal</a></li>
                          <li><a class="dropdown-item" href="<?=base_url('hasil-perlombaan');?>">Hasil Perlombaan</a></li>
                          <li><a class="dropdown-item" href="<?=base_url('live-streaming');?>">Live Streaming</a></li>
                        </ul>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="<?=base_url('download');?>">Download</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div style="background-color:#1eb871">
      <div class="container">
        <div class="row">
          <div class="col-6">
            <div class="row m-2" id="left-logo">
              <div 
                class="
                  col-lg-3 col-md-4 col-sm-12 col-12 
                  d-flex justify-content-center
                "
                id="header-logo-left"
              >
                <img src="<?=base_url('assets/img/logo.png');?>" alt="left logo" id="header-logo-1" class="header-logo">
              </div>
              <div 
                class="
                  col-lg-9 col-md-8 col-sm-12 col-12 
                  mt-2 py-2 px-0 
                  d-flex align-items-center 
                  header-title
                "
              >
                <span class="text-white" id="header-title-1">Portal Berita<br>Islam Terkini</span>
              </div>
            </div>
          </div>
          <div class="col-6">
            <div class="row m-2" id="right-logo">
              <div 
                class="
                  col-lg-9 col-md-8 
                  mt-2 py-2 px-0 
                  d-flex align-items-center justify-content-end
                  header-title
                "
              >
                <span class="text-white header-title-2">Portal Berita<br>Islam Terkini</span>
              </div>
              <div 
                class="
                  col-lg-3 col-md-4 col-4 
                  d-flex justify-content-center 
                "
              >
                <img src="<?=base_url('assets/img/logo.png');?>" alt="right logo" class="header-logo-2 header-logo">
              </div>
            </div>
            <div class="row m-2" id="right-logo-reverse" style="display:none;">
              <div class="col-sm-12 d-flex justify-content-end">
                <img src="<?=base_url('assets/img/logo.png');?>" alt="right logo" class="header-logo-2 header-logo">
              </div>
              <div class="col-sm-12 py-2 px-0 d-flex justify-content-end  header-title">
                <span class="text-white header-title-2">Portal Berita<br>Islam Terkini</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <nav class="navbar navbar-expand-md navbar-dark bg-success" id="navbar-header">
      <div class="container" style="font-weight:bold; color:white !important;">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse d-flex justify-content-center" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item px-2">
              <a class="nav-link" href="<?=base_url();?>">Beranda</a>
            </li>
            <li class="nav-item px-2 dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="berita-dropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Berita
              </a>
              <ul class="dropdown-menu" aria-labelledby="berita-dropdown" id="category-news-menu">
                <li><a class="dropdown-item" href="<?=base_url('berita/nasional');?>">Nasional</a></li>
                <li><a class="dropdown-item" href="<?=base_url('berita/daerah');?>">Daerah</a></li>
              </ul>
            </li>
            <li class="nav-item px-2 dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="profil-dropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Profil
              </a>
              <ul class="dropdown-menu" aria-labelledby="profile-dropdown" id="profile-menu">
                <li><a class="dropdown-item" href="<?=base_url('profile/sejarah');?>">Sejarah</a></li>
                <li><a class="dropdown-item" href="<?=base_url('profile/visi-misi');?>">Visi - Misi</a></li>
                <li><a class="dropdown-item" href="<?=base_url('profile/struktur-organisasi');?>">Struktur Organisasi</a></li>
              </ul>
            </li>
            <li class="nav-item px-2 dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="galeri-dropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Galeri
              </a>
              <ul class="dropdown-menu" aria-labelledby="galeri-dropdown">
                <li><a class="dropdown-item" href="<?=base_url('galeri/foto');?>">Foto</a></li>
                <li><a class="dropdown-item" href="<?=base_url('galeri/video');?>">Video</a></li>
              </ul>
            </li>
            <li class="nav-item px-2 dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="musabaqah-dropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Musabaqah
              </a>
              <ul class="dropdown-menu" aria-labelledby="musabaqah-dropdown">
                <li><a class="dropdown-item" href="<?=base_url('jadwal');?>">Jadwal</a></li>
                <li><a class="dropdown-item" href="<?=base_url('hasil-perlombaan');?>">Hasil Perlombaan</a></li>
                <li><a class="dropdown-item" href="<?=base_url('live-streaming');?>">Live Streaming</a></li>
              </ul>
            </li>
            <li class="nav-item px-2">
              <a class="nav-link" href="<?=base_url('download');?>">Download</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </header>