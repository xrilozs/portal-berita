  <footer>
    <div class="bg-success" id="footer-normal">
      <div class="container py-4">
        <div class="row m-2">
          <div class="col-lg-9 col-md-8 col-sm-8 col-5">
            <div class="row">
              <div class="col-lg-2 col-md-3 col-sm-4 col-12">
                <img src="<?=base_url('assets/img/logo.png');?>" class="footer-logo" alt="" width="120px">
              </div>
              <div class="col-lg-10 col-md-9 col-sm-8 col-12 d-flex align-items-center pl-2" style="text-transform: uppercase;">
                <span class="p-2 text-white footer-title" style="font-size:24px; font-weight:bold;">
                  Portal Berita<br>Islam Terkini
                </span>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-sm-4 col-5">
            <div class="row">
              <div class="col-12 text-white">
                <span style="text-transform:uppercase; font-size:24px; font-weight:bold;">Ikuti Kami</span><br>
                <a href="#" class="btn btn-sm btn-light sm-facebook"><i class="fab fa-facebook-f"></i></a>
                <a href="#" class="btn btn-sm btn-light sm-youtube"><i class="fab fa-youtube"></i></a>
                <a href="#" class="btn btn-sm btn-light sm-twitter"><i class="fab fa-twitter"></i></a>
                <a href="#" class="btn btn-sm btn-light sm-instagram"><i class="fab fa-instagram"></i></a>
              </div>
            </div>
            <div class="row mt-4 text-white">
              <div class="col-12">
                <span style="text-transform:uppercase; font-size:24px; font-weight:bold;">Kontak Kami</span><br>
              </div>
              <div class="col-lg-12 col-md-12 col-12 mb-4" style="font-size:12px;">
                <div class="row mt-2">
                  <div class="col-1">
                    <i class="fas fa-map-marker-alt"></i>
                  </div>
                  <div class="col">
                    <span class="contact-address"></span>
                  </div>
                </div>
                <div class="row mt-2">
                  <div class="col-1">
                    <i class="fas fa-phone"></i>
                  </div>
                  <div class="col">
                    <span class="contact-phone"></span>
                  </div>
                </div>
                <div class="row mt-2">
                  <div class="col-1">
                    <i class="fas fa-envelope"></i>
                  </div>
                  <div class="col">
                    <span class="contact-email"></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="bg-success" id="footer-mobile" style="display: none;">
      <div class="container py-4">
        <div class="row m-2">
          <div class="col-6">
            <div class="row">
              <div class="col-12">
                <img src="<?=base_url('assets/img/logo.png');?>" class="footer-logo" alt="" width="80">
              </div>
              <div class="col-12">
                <span class="p-2 text-white footer-title" style="font-size:24px; font-weight:bold;">
                  Portal Berita<br>Islam Terkini
                </span>
              </div>
            </div>
          </div>
          <div class="col-6">
            <div class="row">
              <div class="col-12 text-white">
                <span style="text-transform:uppercase; font-size:24px; font-weight:bold;">Ikuti Kami</span><br>
                <a href="#" class="btn btn-sm btn-light"><i class="fab fa-facebook-f"></i></a>
                <a href="#" class="btn btn-sm btn-light"><i class="fab fa-youtube"></i></a>
                <a href="#" class="btn btn-sm btn-light"><i class="fab fa-twitter"></i></a>
                <a href="#" class="btn btn-sm btn-light"><i class="fab fa-instagram"></i></a>
              </div>
            </div>
            <div class="row text-white mt-4">
              <div class="col-12">
                <span style="text-transform:uppercase; font-size:24px; font-weight:bold;">Kontak Kami</span><br>
              </div>
              <div class="col-lg-12 col-md-12 col-12 mb-4">
                <div class="row mt-2">
                  <div class="col-1">
                    <i class="fas fa-map-marker-alt"></i>
                  </div>
                  <div class="col">
                    <span class="contact-address"></span>
                  </div>
                </div>
                <div class="row mt-2">
                  <div class="col-1">
                    <i class="fas fa-phone"></i>
                  </div>
                  <div class="col">
                    <span class="contact-phone"></span>
                  </div>
                </div>
                <div class="row mt-2">
                  <div class="col-1">
                    <i class="fas fa-envelope"></i>
                  </div>
                  <div class="col">
                    <span class="contact-email"></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="bg-dark text-white d-flex justify-content-center py-2" style="font-weight:bold;">
      &copy; Copyright 2023&nbsp;<span id="footer-brand"><?=COMPANY_NAME;?></span>
    </div>
  </footer>

  <!-- Vendor CSS Files -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.6.3.min.js" integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script> -->
  <!-- <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script> -->
  <script src="<?=base_url('assets/vendor/slick/slick/slick.js');?>"></script>
  <script type="text/javascript" src="//cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
  <!-- Custom JS -->
  <script>
    const WEB_URL = '<?=BASE_URL;?>';
    const API_URL = '<?=API_URL;?>';
  </script>
  <script type="text/javascript" src="<?=base_url('assets/js/main.js');?>?v=<?=date(ASSET_VERSION);?>"></script>
  <?= $title == 'beranda' ? '<script type="text/javascript" src="'.base_url('assets/js/home.js').'?v='.date(ASSET_VERSION).'"></script>' : ''; ?>
  <?= $title == 'berita' ? '<script type="text/javascript" src="'.base_url('assets/js/news.js').'?v='.date(ASSET_VERSION).'"></script>' : ''; ?>
  <?= $title == 'berita detail' ? '<script type="text/javascript" src="'.base_url('assets/js/news_detail.js').'?v='.date(ASSET_VERSION).'"></script>' : ''; ?>
  <?= $title == 'download' ? '<script type="text/javascript" src="'.base_url('assets/js/download.js').'?v='.date(ASSET_VERSION).'"></script>' : ''; ?>
  <?= $title == 'galeri foto' ? '<script type="text/javascript" src="'.base_url('assets/js/photo.js').'?v='.date(ASSET_VERSION).'"></script>' : ''; ?>
  <?= $title == 'galeri foto detail' ? '<script type="text/javascript" src="'.base_url('assets/js/photo_detail.js').'?v='.date(ASSET_VERSION).'"></script>' : ''; ?>
  <?= $title == 'galeri video' ? '<script type="text/javascript" src="'.base_url('assets/js/video.js').'?v='.date(ASSET_VERSION).'"></script>' : ''; ?>
  <?= $title == 'galeri video detail' ? '<script type="text/javascript" src="'.base_url('assets/js/video_detail.js').'?v='.date(ASSET_VERSION).'"></script>' : ''; ?>
  <?= $title == 'profil' ? '<script type="text/javascript" src="'.base_url('assets/js/profile.js').'?v='.date(ASSET_VERSION).'"></script>' : ''; ?>
  <?= $title == 'kontak kami' ? '<script type="text/javascript" src="'.base_url('assets/js/contact_us.js').'?v='.date(ASSET_VERSION).'"></script>' : ''; ?>
  <?= $title == 'hasil perlombaan' ? '<script type="text/javascript" src="'.base_url('assets/js/competition_result.js').'?v='.date(ASSET_VERSION).'"></script>' : ''; ?>
  <?= $title == 'jadwal perlombaan' ? '<script type="text/javascript" src="'.base_url('assets/js/competition_schedule.js').'?v='.date(ASSET_VERSION).'"></script>' : ''; ?>
  <?= $title == 'live streaming' ? '<script type="text/javascript" src="'.base_url('assets/js/live_streaming.js').'?v='.date(ASSET_VERSION).'"></script>' : ''; ?>
</body>
</html>