<div class="container my-4" style="min-height:380px;">
  <?php if($is_sidebar) : ?>
  <div class="row">
    <div class="col-lg-8 col-md-7 col-12 mb-4">
      <?=$content;?>
    </div>
    <div class="col-lg-4 col-md-5 col-12 mb-4">
      <?=$sidebar;?>
    </div>
  </div>
  <?php else : ?>
  <div class="row">
    <div class="col-12">
      <?=$content;?>
    </div>
  </div>
  <?php endif; ?>

</div>