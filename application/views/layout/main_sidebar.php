<div class="card bg-light" style="display:none;">
  <h5 class="card-title text-center pt-2">Jadwal Shalat</h5>
  <div class="card-body">
    <div class="row" style="font-size:13px;">
      <div class="col-lg-2 col-md-4 col-2 mb-2">
        <label><strong>Imsak</strong></label><br>04:18
      </div>
      <div class="col-lg-2 col-md-4 col-2 mb-2">
        <label><strong>Subuh</strong></label><br>04:28
      </div>
      <div class="col-lg-2 col-md-4 col-2 mb-2">
        <label><strong>Dzuhur</strong></label><br>12:06
      </div>
      <div class="col-lg-2 col-md-4 col-2 mb-2">
        <label><strong>Ashar</strong></label><br>15:30
      </div>
      <div class="col-lg-2 col-md-4 col-2 mb-2">
        <label><strong>Magrib</strong></label><br>18:19
      </div>
      <div class="col-lg-2 col-md-4 col-2 mb-2">
        <label><strong>Isya</strong></label><br>19:33
      </div>
    </div>
  </div>
</div>

<div class="card bg-light" id="apps-content">
  <h5 class="card-title text-center pt-2">Aplikasi</h5>
  <div class="card-body">
    <div class="row">
      <div class="col-12 d-flex justify-content-center">
        <div class="slide-apps" style="width:90%;" id="sidebar-apps">
          <?=$loading;?>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="card bg-light mt-4">
  <h5 class="card-title text-center pt-2">Berita Populer</h5>
  <div class="card-body">
    <div class="row" id="sidebar-news">
      <?=$loading;?>
    </div>
  </div>
</div>