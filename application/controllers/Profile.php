<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function index($page)
	{
		$data_content['title']				= strtoupper(str_replace("-", " ", $page));
		$data_content['loading'] 			= $this->load->view('component/skeleton_loading', [], true);
		$data_page['content'] 				= $this->load->view('page/profile/index', $data_content, true);
		$data_page['sidebar'] 				= $this->load->view('layout/main_sidebar', $data_content, true);
		$data_page['is_sidebar']			= 1;
		$data_page['title']						= "profil";

		$this->load->view('layout/main_header', $data_page);
		$this->load->view('layout/main_body', $data_page);
		$this->load->view('layout/main_footer', $data_page);
	}
}
