<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Competition extends CI_Controller {

	public function schedule()
	{
		$data_content['loading'] 			= $this->load->view('component/skeleton_loading', [], true);
		$data_page['content'] 				= $this->load->view('page/competition/schedule', $data_content, true);
		$data_page['is_sidebar']			= 0;
		$data_page['title']						= "jadwal perlombaan";

		$this->load->view('layout/main_header', $data_page);
		$this->load->view('layout/main_body', $data_page);
		$this->load->view('layout/main_footer', $data_page);
	}

	public function result()
	{
		$data_page['content'] 				= $this->load->view('page/competition/result', [], true);
		$data_page['is_sidebar']			= 0;
		$data_page['title']						= "hasil perlombaan";

		$this->load->view('layout/main_header', $data_page);
		$this->load->view('layout/main_body', $data_page);
		$this->load->view('layout/main_footer', $data_page);
	}


	public function live_streaming()
	{
		$data_content['loading'] 			= $this->load->view('component/skeleton_loading', [], true);
		$data_page['content'] 				= $this->load->view('page/competition/live_streaming', $data_content, true);
		$data_page['is_sidebar']			= 0;
		$data_page['title']						= "live streaming";


		$this->load->view('layout/main_header', $data_page);
		$this->load->view('layout/main_body', $data_page);
		$this->load->view('layout/main_footer', $data_page);
	}
}
