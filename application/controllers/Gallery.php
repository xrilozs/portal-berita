<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function photo()
	{
		$data_content['loading'] 			= $this->load->view('component/skeleton_loading', [], true);
		$data_page['content'] 				= $this->load->view('page/gallery/photo', $data_content, true);
		$data_page['sidebar'] 				= $this->load->view('layout/main_sidebar', $data_content, true);
		$data_page['is_sidebar']			= 1;
		$data_page['title']						= "galeri foto";

		$this->load->view('layout/main_header', $data_page);
		$this->load->view('layout/main_body', $data_page);
		$this->load->view('layout/main_footer', $data_page);
	}

	public function video()
	{
		$data_content['loading'] 			= $this->load->view('component/skeleton_loading', [], true);
		$data_page['content'] 				= $this->load->view('page/gallery/video', $data_content, true);
		$data_page['sidebar'] 				= $this->load->view('layout/main_sidebar', $data_content, true);
		$data_page['is_sidebar']			= 1;
		$data_page['title']						= "galeri video";

		$this->load->view('layout/main_header', $data_page);
		$this->load->view('layout/main_body', $data_page);
		$this->load->view('layout/main_footer', $data_page);
	}


	public function photo_detail($alias)
	{
		$data_content['photo_title']	= strtoupper(str_replace("-", " ", $alias));
		$data_content['loading'] 			= $this->load->view('component/skeleton_loading', [], true);
		$data_page['content'] 				= $this->load->view('page/gallery/photo_detail', $data_content, true);
		$data_page['sidebar'] 				= $this->load->view('layout/main_sidebar', $data_content, true);
		$data_page['is_sidebar']			= 1;
		$data_page['title']						= "galeri foto detail";


		$this->load->view('layout/main_header', $data_page);
		$this->load->view('layout/main_body', $data_page);
		$this->load->view('layout/main_footer', $data_page);
	}

	public function video_detail($alias)
	{
		$data_content['video_title']	= strtoupper(str_replace("-", " ", $alias));
		$data_content['loading'] 			= $this->load->view('component/skeleton_loading', [], true);
		$data_page['content'] 				= $this->load->view('page/gallery/video_detail', $data_content, true);
		$data_page['sidebar'] 				= $this->load->view('layout/main_sidebar', $data_content, true);
		$data_page['is_sidebar']			= 1;
		$data_page['title']						= "galeri video detail";

		$this->load->view('layout/main_header', $data_page);
		$this->load->view('layout/main_body', $data_page);
		$this->load->view('layout/main_footer', $data_page);
	}
}
