<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_us extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function index()
	{
		$data_page['content'] 				= $this->load->view('page/contact_us/index', [], true);
		$data_page['sidebar'] 				= $this->load->view('layout/main_sidebar', [], true);
		$data_page['is_sidebar']			= 0;
		$data_page['title']						= "kontak kami";

		$this->load->view('layout/main_header', $data_page);
		$this->load->view('layout/main_body', $data_page);
		$this->load->view('layout/main_footer', $data_page);
	}
}
