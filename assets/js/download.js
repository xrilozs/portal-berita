let PAGE_NUMBER = 1

$(document).ready(function(){
  get_document()

  $("body").delegate(".document-pagination-item", "click", function(e){
      e.preventDefault()
      e.stopImmediatePropagation()
  
      let page = $(this).data('page')
      PAGE_NUMBER = page
      console.log('page:',PAGE_NUMBER)
      get_document()
      window.scrollTo(0, 0);
  });

  $("body").delegate(".document-pagination-previous", "click", function(e){
      e.preventDefault()
      e.stopImmediatePropagation()

      PAGE_NUMBER -= 1
      console.log('page:',PAGE_NUMBER)
      get_document()
      window.scrollTo(0, 0);
  })

  $("body").delegate(".document-pagination-next", "click", function(e){
      e.preventDefault()
      e.stopImmediatePropagation()

      PAGE_NUMBER += 1
      console.log('page:',PAGE_NUMBER)
      get_document()
      window.scrollTo(0, 0);
  })
})

function get_document(){
  $.ajax({
    async: true,
    url: `${DOCUMENT_API_URL}?page_number=${PAGE_NUMBER-1}&page_size=10&status=PUBLISH`,
    type: 'GET',
    error: function(res) {
      const response  = JSON.parse(res.responseText)
      // let isRetry     = retryRequest(response)
      // if(isRetry) $.ajax(this)
      renderDocument([])
      renderDocumentPagination(0)
    },
    success: function(res) {
      const response  = res.data
      const totalPage = Math.ceil(res.recordsTotal/10)

      renderDocument(response)
      renderDocumentPagination(totalPage)
    }
  });
}

function renderDocument(document){
  let documentHtml = ``
  for(const item of document){
    const itemHtml = `<div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-2 d-flex justify-content-center">
              <i class="far fa-file-pdf fa-5x"></i>
            </div>
            <div class="col-7">
              <h4>${item.name}</h4>
              <span>Ukuran: ${item.size}</span>
            </div>
            <div class="col-3">
              <span class="float-end" style="font-size:13px;">${format_date_ID(item.created_at)}</span>
              <a href="${item.full_document_url}" class="btn btn-success float-end mt-2" target="_blank">
                <i class="fas fa-cloud-download-alt"></i> Download
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>`
    documentHtml += itemHtml
  }

  if(documentHtml == ''){
    documentHtml = `<div class="col-12 d-flex justify-content-center"><i>Belum Ada dokumen</i></div>`
  }

  $('#list-document').html(documentHtml)
}

function renderDocumentPagination(totalPage){
    if(totalPage > 1){
        let fullPagesHtml = ``
        let previousDisable = PAGE_NUMBER == 1 ? 'disabled' : ''
        let nextDisable = PAGE_NUMBER == totalPage ? 'disabled' : ''
        let isPageLimit = totalPage > 5 ? true : false;

        let previousHtml = `<li class="page-item ${previousDisable}">
            <a class="page-link document-pagination-previous" href="#">
                <i class="fas fa-chevron-left"></i>
            </a>
        </li>`
        let nextHtml = `<li class="page-item ${nextDisable}">
            <a class="page-link document-pagination-next" href="#">
                <i class="fas fa-chevron-right"></i>
            </a>
        </li>`
        
        let pagesHtml = ``
        for(let i=1; i<=totalPage; i++){
            const pageActive = i == PAGE_NUMBER ? 'active' : ''
            const pageDisabled = i == PAGE_NUMBER ? '' : 'href="#"'
            let pageHtml = ``
            if(isPageLimit){
                if(i == PAGE_NUMBER){
                    pageHtml = `<li class="page-item ${pageActive} ${pageDisabled}">
                        <a class="page-link document-pagination-item" data-page="${i}" ${pageDisabled}>${i}</a>
                    </li>`
                }else if(i == PAGE_NUMBER-1 || i == PAGE_NUMBER+1){
                    pageHtml = `<li class="page-item ${pageActive} ${pageDisabled}">
                        <a class="page-link document-pagination-item" data-page="${i}" ${pageDisabled}>${i}</a>
                    </li>`
                }else if(i == PAGE_NUMBER-10 || i == PAGE_NUMBER+10){
                    pageHtml = `<li class="page-item disabled">
                        <span class="page-link document-pagination-item">...</span>
                    </li>`
                }
            }else{
                pageHtml = `<li class="page-item ${pageActive} ${pageDisabled}">
                    <a class="page-link document-pagination-item" data-page="${i}" ${pageDisabled}>${i}</a>
                </li>`
            }
            pagesHtml += pageHtml
        }
        fullPagesHtml = `${previousHtml}${pagesHtml}${nextHtml}`
        $(`#document-pagination`).html(fullPagesHtml)
    }else{
        $(`#document-pagination`).html("")
    }
    
}
