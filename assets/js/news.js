let CATEGORY    = window.location.pathname.split("/").pop(),
    PAGE_NUMBER = 1

$(document).ready(function(){
  get_slider_news(CATEGORY)
  get_news()

  $("body").delegate(".news-pagination-item", "click", function(e){
      e.preventDefault()
      e.stopImmediatePropagation()
  
      let page = $(this).data('page')
      PAGE_NUMBER = page
      console.log('page:',PAGE_NUMBER)
      get_news()
      window.scrollTo(0, 0);
  });

  $("body").delegate(".news-pagination-previous", "click", function(e){
      e.preventDefault()
      e.stopImmediatePropagation()

      PAGE_NUMBER -= 1
      console.log('page:',PAGE_NUMBER)
      get_news()
      window.scrollTo(0, 0);
  })

  $("body").delegate(".news-pagination-next", "click", function(e){
      e.preventDefault()
      e.stopImmediatePropagation()

      PAGE_NUMBER += 1
      console.log('page:',PAGE_NUMBER)
      get_news()
      window.scrollTo(0, 0);
  })
})

function get_news(){
  $.ajax({
    async: true,
    url: `${NEWS_API_URL}?page_number=${PAGE_NUMBER-1}&page_size=10&category_news_alias=${CATEGORY}&status=PUBLISH`,
    type: 'GET',
    error: function(res) {
      const response  = JSON.parse(res.responseText)
      // let isRetry     = retryRequest(response)
      // if(isRetry) $.ajax(this)
      render_not_found()
      renderNewsPagination(0)
    },
    success: function(res) {
      const response  = res.data
      const totalPage = Math.ceil(res.recordsTotal/10)

      renderNews(response)
      renderNewsPagination(totalPage)
    }
  });
}

function renderNews(news){
  let newsHtml = ``
  for(const item of news){
    let title = item.title.length > 30 ? item.title.slice(0, 30) + "..." : item.title
    let category = item.category_news_name.toLowerCase()
    const link = `${WEB_URL}berita/${category}/${item.alias}`
    const itemHtml = `<div class="col-lg-6 col-md-12 col-sm-6 col-12 my-3">
      <div class="row">
        <div class="col-xl-4 col-lg-5 col-md-4 col-sm-5 col-4">
          <img src="${item.full_img_url}" style="width:100px;height:90px; object-fit: cover"><br>
        </div>
        <div class="col-xl-8 col-lg-7 col-md-8 col-sm-7 col-8">
          <div class="row">
            <div class="col-12">
              <p>
                <a href="${link}">${title}</a>
              </p>
            </div>
            <div class="col-12">
              <span class="text-success" style="font-size:12px;">${item.created_at}</span>
            </div>
          </div>
        </div>
      </div>
    </div>`
    newsHtml += itemHtml
  }

  if(newsHtml == ''){
    newsHtml = `<div class="col-12 d-flex justify-content-center"><i>Belum Ada Berita</i></div>`
  }

  $('#list-news').html(newsHtml)
}

function renderNewsPagination(totalPage){
    if(totalPage > 1){
        let fullPagesHtml = ``
        let previousDisable = PAGE_NUMBER == 1 ? 'disabled' : ''
        let nextDisable = PAGE_NUMBER == totalPage ? 'disabled' : ''
        let isPageLimit = totalPage > 5 ? true : false;

        let previousHtml = `<li class="page-item ${previousDisable}">
            <a class="page-link news-pagination-previous" href="#">
                <i class="fas fa-chevron-left"></i>
            </a>
        </li>`
        let nextHtml = `<li class="page-item ${nextDisable}">
            <a class="page-link news-pagination-next" href="#">
                <i class="fas fa-chevron-right"></i>
            </a>
        </li>`
        
        let pagesHtml = ``
        for(let i=1; i<=totalPage; i++){
            const pageActive = i == PAGE_NUMBER ? 'active' : ''
            const pageDisabled = i == PAGE_NUMBER ? '' : 'href="#"'
            let pageHtml = ``
            if(isPageLimit){
                if(i == PAGE_NUMBER){
                    pageHtml = `<li class="page-item ${pageActive} ${pageDisabled}">
                        <a class="page-link news-pagination-item" data-page="${i}" ${pageDisabled}>${i}</a>
                    </li>`
                }else if(i == PAGE_NUMBER-1 || i == PAGE_NUMBER+1){
                    pageHtml = `<li class="page-item ${pageActive} ${pageDisabled}">
                        <a class="page-link news-pagination-item" data-page="${i}" ${pageDisabled}>${i}</a>
                    </li>`
                }else if(i == PAGE_NUMBER-10 || i == PAGE_NUMBER+10){
                    pageHtml = `<li class="page-item disabled">
                        <span class="page-link news-pagination-item">...</span>
                    </li>`
                }
            }else{
                pageHtml = `<li class="page-item ${pageActive} ${pageDisabled}">
                    <a class="page-link news-pagination-item" data-page="${i}" ${pageDisabled}>${i}</a>
                </li>`
            }
            pagesHtml += pageHtml
        }
        fullPagesHtml = `${previousHtml}${pagesHtml}${nextHtml}`
        $(`#news-pagination`).html(fullPagesHtml)
    }else{
        $(`#news-pagination`).html("")
    }
    
}

function render_not_found(){
  $('#news-category').css('display', 'none')
  $('#list-news').html('<p align="center"><i>Halaman tidak ditemukan</i></p>')
}
