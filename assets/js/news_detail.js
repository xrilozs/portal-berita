let TITLE = window.location.pathname.split("/").pop()

$(document).ready(function(){
  get_news_detail()
})

function get_news_detail(){
  $.ajax({
    async: true,
    url: `${NEWS_API_URL}by-alias/${TITLE}`,
    type: 'GET',
    error: function(res) {
      const response  = JSON.parse(res.responseText)
      // let isRetry     = retryRequest(response)
      // if(isRetry) $.ajax(this)
      render_not_found()
      render_news_related([])
    },
    success: function(res) {
      const response  = res.data

      render_news_detail(response)
      get_news_related(response.category_news_alias)
    }
  });
}

function get_news_related(category){
  $.ajax({
    async: true,
    url: `${NEWS_API_URL}?page_number=0&page_size=10&category_news_alias=${category}`,
    type: 'GET',
    error: function(res) {
      const response  = JSON.parse(res.responseText)
      // let isRetry     = retryRequest(response)
      // if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const response  = res.data

      render_news_related(response)
    }
  });
}

function render_news_detail(news){
  //image
  $('#news-image').prop("src", news.full_img_url)
  $('#news-date').html(format_date_ID(news.created_at))
  $('#news-title').html(news.title)
  $('#news-content').html(news.content)
}

function render_news_related(news){
  let newsHtml = ``
  for(const item of news){
    let title = item.title.length > 30 ? item.title.slice(0, 30) + "..." : item.title
    let category = item.category_news_name.toLowerCase()
    const link = `${WEB_URL}berita/${category}/${item.alias}`
    const itemHtml = `<div class="col-lg-6 col-md-12 col-sm-6 col-12 my-3">
      <div class="row">
        <div class="col-xl-4 col-lg-5 col-md-4 col-sm-5 col-4">
          <img src="${item.full_img_url}" style="width:100px;height:90px; object-fit: cover"><br>
        </div>
        <div class="col-xl-8 col-lg-7 col-md-8 col-sm-7 col-8">
          <div class="row">
            <div class="col-12">
              <p>
                <a href="${link}">${title}</a>
              </p>
            </div>
            <div class="col-12">
              <span class="text-success" style="font-size:12px;">${item.created_at}</span>
            </div>
          </div>
        </div>
      </div>
    </div>`
    newsHtml += itemHtml
  }

  $('#related-news').html(newsHtml)
}

function render_not_found(){
  $('#news-image-section').css('display', 'none')
  $('#news-nav-section').css('display', 'none')
  $('#news-title').css('display', 'none')
  $('#news-related-header').css('display', 'none')
  $('#news-content').html('<p align="center"><i>Halaman tidak ditemukan</i></p>')
}
