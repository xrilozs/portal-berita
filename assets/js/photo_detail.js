let TITLE = window.location.pathname.split("/").pop()

$(document).ready(function(){
  get_photo_detail()

  $('.slider-single').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav'
  });
 
  $('.slider-nav')
    .on('init', function(event, slick) {
      $('.slider-nav .slick-slide.slick-current').addClass('is-active');
    })
    .slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      asNavFor: '.slider-single',
      // centerMode: true,
      focusOnSelect: true,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true,
          },
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true,
          },
        },
      ],
    });
 
  $('.slider-single').on('afterChange', function(event, slick, currentSlide) {
    $('.slider-nav').slick('slickGoTo', currentSlide);
    var currrentNavSlideElem = '.slider-nav .slick-slide[data-slick-index="' + currentSlide + '"]';
    $('.slider-nav .slick-slide.is-active').removeClass('is-active');
    $(currrentNavSlideElem).addClass('is-active');
  });
 
  $('.slider-nav').on('click', '.slick-slide', function(event) {
    event.preventDefault();
    var goToSingleSlide = $(this).data('slick-index');
 
    $('.slider-single').slick('slickGoTo', goToSingleSlide);
  });
});

function get_photo_detail(){
  $.ajax({
    async: false,
    url: `${PHOTO_API_URL}by-alias/${TITLE}`,
    type: 'GET',
    error: function(res) {
      const response  = JSON.parse(res.responseText)
      // let isRetry     = retryRequest(response)
      // if(isRetry) $.ajax(this)
      render_not_found()
      render_photo_slider([])
    },
    success: function(res) {
      const response  = res.data

      render_photo_slider(response.items)
      render_photo_detail(response)
    }
  });
}

function render_photo_detail(photo){
  //image
  $('#photo-date').html(format_date_ID(photo.created_at))
  $('#photo-title').html(photo.title)
  $('#photo-description').html("Deskripsi: "+photo.description)
}

function render_photo_slider(photo){
  let display_html = ''
  let slider_html = ''
  photo.forEach(item => {
    let display_item_html = `<div><img src="${item.full_photo_url}" style="width:100%;height:400px; object-fit: cover"></div>`
    let slider_item_html = `<div class="mx-2"><img src="${item.full_photo_url}" style="width:150px;height:100px; object-fit: cover"></div>`

    display_html += display_item_html
    slider_html += slider_item_html
  });

  $('#photo-display').html(display_html)
  $('#photo-slider').html(slider_html)
}

function render_not_found(){
  $('#photo-nav-section').css('display', 'none')
  $('#photo-title').css('display', 'none')
  $('#photo-content').css('display', 'none')
  $('#photo-description').html('<p align="center"><i>Halaman tidak ditemukan</i></p>')
}
