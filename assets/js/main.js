let CONFIG_API_URL        = API_URL + "config/"
let APPS_API_URL          = API_URL + "apps/"
let NEWS_API_URL          = API_URL + "news/"
let CATEGORY_NEWS_API_URL = API_URL + "category_news/"
let PROFILE_API_URL       = API_URL + "profile/"
let PHOTO_API_URL         = API_URL + "photo/"
let VIDEO_API_URL         = API_URL + "video/"
let DOCUMENT_API_URL      = API_URL + "document/"
let LIVE_STREAMING_API_URL= API_URL + "live_streaming/"
let SCHEDULE_API_URL      = API_URL + "competition_schedule/"
let SCORE_API_URL         = API_URL + "competition_score/"

$(document).ready(function(){
  //sidebar apps
  // $('.slide-apps').slick({
  //   infinite: true,
  //   slidesToShow: 3,
  //   slidesToScroll: 1
  // }) 

  get_config()
  get_category_news()
  get_profile_page()
  get_sidebar_news()
  get_sidebar_apps()
  render_header_date()
})

function get_config(){
  $.ajax({
    async: true,
    url: CONFIG_API_URL,
    type: 'GET',
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      let config = res.data
      if(config){
        renderWebConfig(config)
        renderSosmed(config)
        renderContactConfig(config)
      }
      
    }
  });
}

function get_category_news(){
  $.ajax({
    async: true,
    url: `${CATEGORY_NEWS_API_URL}all`,
    type: 'GET',
    error: function(res) {
      const response = JSON.parse(res.responseText)
      // let isRetry = retryRequest(response)
      // if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const response = res.data
      render_category_news_menu(response)
    }
  });
}

function get_profile_page(){
  $.ajax({
    async: true,
    url: `${PROFILE_API_URL}all`,
    type: 'GET',
    error: function(res) {
      const response = JSON.parse(res.responseText)
      // let isRetry = retryRequest(response)
      // if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const response = res.data
      render_profile_menu(response)
    }
  });
}

function get_sidebar_news(){
  $.ajax({
    async: true,
    url: `${NEWS_API_URL}?page_number=0&page_size=5&status=PUBLISH`,
    type: 'GET',
    error: function(res) {
      const response = JSON.parse(res.responseText)
      // let isRetry = retryRequest(response)
      // if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const response = res.data
      render_sidebar_news(response)
    }
  });
}

function get_sidebar_apps(){
  $.ajax({
    async: true,
    url: `${APPS_API_URL}all`,
    type: 'GET',
    error: function(res) {
      const response = JSON.parse(res.responseText)
      // let isRetry = retryRequest(response)
      // if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const response = res.data
      render_sidebar_apps(response)
    }
  });
}

function get_slider_news(category){
  let url = `${NEWS_API_URL}?page_number=0&page_size=5&status=PUBLISH`
  if(category) url += `&category_news_alias=${category}`
  $.ajax({
    async: true,
    url: url,
    type: 'GET',
    error: function(res) {
      const response = JSON.parse(res.responseText)
      // let isRetry = retryRequest(response)
      // if(isRetry) $.ajax(this)
      render_slider_news([])
    },
    success: function(res) {
      const response = res.data
      render_slider_news(response)
    }
  });
}

function renderWebConfig(config){
  if(config.title_1){
    $('#header-title-1').html(config.title_1)
    $('.footer-title').html(config.title_1)
    $('#footer-brand').html(config.title_1)
  }
  if(config.title_2){
    $('.header-title-2').html(config.title_2)
  }
  if(config.logo_1_url){
    $('#header-logo-1').prop("src", API_URL+config.logo_1_url)
    $('.footer-logo').prop("src", API_URL+config.logo_1_url)
  }
  if(config.logo_2_url){
    $('.header-logo-2').prop("src", API_URL+config.logo_2_url)
  }
  if(config.icon_url){
    $("#favicon").attr("href", API_URL+config.icon_url);
  }
}

function renderContactConfig(config){
  let map = config.google_map
  map = map.replace('width="600"', 'width="100%"')
  map = map.replace('height="450"', 'height="200px"')
  $('.contact-address').html(config.address)
  $('.contact-phone').html(config.phone)
  $('.contact-email').html(config.email)
  $('.contact-map').html(map)
}

function renderSosmed(config){
  if(config.sm_youtube){
    $('.sm-youtube').prop('href', format_url(config.sm_youtube))
  }
  if(config.sm_facebook){
    $('.sm-facebook').prop('href', format_url(config.sm_facebook))
  }
  if(config.sm_instagram){
    $('.sm-instagram').prop('href', format_url(config.sm_instagram))
  }
  if(config.sm_twitter){
    $('.sm-twitter').prop('href', format_url(config.sm_twitter))
  }
}

function render_category_news_menu(category_news){
  let category_news_html = ''
  category_news.forEach(item => {
    let item_html = `<li><a class="dropdown-item" href="${WEB_URL}berita/${item.alias}">${item.name}</a></li>`
    category_news_html += item_html
  });

  $('#category-news-menu').html(category_news_html)
  $('#category-news-menu-mobile').html(category_news_html)
}

function render_profile_menu(profile){
  let profile_html = ''
  profile.forEach(item => {
    let item_html = `<li><a class="dropdown-item" href="${WEB_URL}profile/${item.alias}">${item.title}</a></li>`
    profile_html += item_html
  });

  $('#profile-menu').html(profile_html)
  $('#profile-menu-mobile').html(profile_html)
}

function render_sidebar_news(news){
  let newsHtml = ``
  for(const item of news){
    let title = item.title.length > 30 ? item.title.slice(0, 30) + "..." : item.title
    let category = item.category_news_name.toLowerCase()
    const link = `${WEB_URL}berita/${category}/${item.alias}`
    const itemHtml = `<div class="col-12 mb-2">
      <div class="row">
        <div class="col-xl-4 col-lg-5 col-md-6 col-sm-4 col-4">
          <img src="${item.full_img_url}" style="width:100px;height:80px; object-fit: cover"><br>
        </div>
        <div class="col-xl-8 col-lg-7 col-md-6 col-sm-8 col-8">
          <div class="row">
            <div class="col-12">
              <p>
                <a href="${link}">${title}</a>
              </p>
            </div>
            <div class="col-12">
              <span class="text-success" style="font-size:12px;">${item.created_at}</span>
            </div>
          </div>
        </div>
      </div>
    </div>`
    newsHtml += itemHtml
  }

  if(newsHtml == ''){
    newsHtml = `<div class="col-12 d-flex justify-content-center"><i>Belum Ada Berita</i></div>`
  }

  $('#sidebar-news').html(newsHtml)
}

function render_sidebar_apps(apps){
  let apps_html = ''
  apps.forEach(item => {
    let item_html = `<div>
      <div class="row">
        <div class="col-12 d-flex justify-content-center">
          <a href="${item.url}">
            <img src="${item.full_icon_url}" class="circular_image"><br>
          </a>
        </div>
        <div class="col-12 d-flex justify-content-center">
          <a href="${item.url}">${item.name}</a>
        </div>
      </div>
    </div>`
    apps_html += item_html
  });

  if(apps_html == ''){
    $('#apps-content').hide()
  }else{
    $('#sidebar-apps').html(apps_html)
    $('.slide-apps').slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1
    }) 
  }
}

function render_slider_news(news){
  let newsHtml = ``
  let index = 1;
  for(const item of news){
  let is_active = index == 1 ? 'active' : ''
  index++
  let category = item.category_news_name.toLowerCase()
  const link = `${WEB_URL}berita/${category}/${item.alias}`
  const itemHtml = `<div class="carousel-item ${is_active}" data-bs-interval="2000">
      <a href="${link}">
      <img src="${item.full_img_url}" class="d-block w-100" style="height: 500px; object-fit: cover;" alt="...">
      </a>  
      <div class="carousel-caption" style="backdrop-filter: blur(1px);">
      <span class="text-white" style="font-weight:bold; font-size:1.5vw; text-shadow: 0 0 2px black;">
          ${item.category_news_name}
      </span>
      <h3 class="text-white" style="font-weight:bold; font-size:2vw; text-shadow: 0 0 3px black;">
          ${item.title}
      </h3>
      <span class="text-white" style="font-weight:bold; font-size:1.5vw; text-shadow: 0 0 2px black;">
          ${item.created_at}
      </span>
      </div>
  </div>`
  newsHtml += itemHtml
  }

  if(newsHtml == ''){
    $('#slider-content').html('')
  }else{
    $('#slider-news').html(newsHtml)
  }

}

function render_header_date(){
  $('#header-date-left').html(format_date_ID())
  $('#header-date-right').html(format_date_ID())
}

function format_date_ID(date_str=null){

  let day_arr = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu']
  let month_arr = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']

  let datetime = date_str ? new Date(date_str) : new Date(),
      day = day_arr[datetime.getDay()],
      date = datetime.getDate(),
      month = month_arr[datetime.getMonth()],
      year = datetime.getFullYear()

  return `${day}, ${date} ${month} ${year}`
}

function format_url(url){
  if(url.includes('http://') || url.includes('https://')){
    return url
  }else{
    return `https://${url}`
  }
}

//fixed top navbar on scroll
window.addEventListener('scroll', function() {
  if (window.scrollY > 100) {
    document.getElementById('navbar-header').classList.add('fixed-top');
    // add padding top to show content behind navbar
    navbar_height = document.querySelector('.navbar').offsetHeight;
    document.body.style.paddingTop = navbar_height + 'px';
  } else {
    document.getElementById('navbar-header').classList.remove('fixed-top');
     // remove padding top from body
    document.body.style.paddingTop = '0';
  } 
});