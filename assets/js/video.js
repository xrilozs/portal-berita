let PAGE_NUMBER = 1

$(document).ready(function(){
  get_video()

  $("body").delegate(".video-pagination-item", "click", function(e){
      e.preventDefault()
      e.stopImmediatePropagation()
  
      let page = $(this).data('page')
      PAGE_NUMBER = page
      console.log('page:',PAGE_NUMBER)
      get_video()
      window.scrollTo(0, 0);
  });

  $("body").delegate(".video-pagination-previous", "click", function(e){
      e.preventDefault()
      e.stopImmediatePropagation()

      PAGE_NUMBER -= 1
      console.log('page:',PAGE_NUMBER)
      get_video()
      window.scrollTo(0, 0);
  })

  $("body").delegate(".video-pagination-next", "click", function(e){
      e.preventDefault()
      e.stopImmediatePropagation()

      PAGE_NUMBER += 1
      console.log('page:',PAGE_NUMBER)
      get_video()
      window.scrollTo(0, 0);
  })
})

  function get_video(){
    $.ajax({
      async: true,
      url: `${VIDEO_API_URL}?page_number=${PAGE_NUMBER-1}&page_size=10&status=PUBLISH`,
      type: 'GET',
      error: function(res) {
        const response  = JSON.parse(res.responseText)
        // let isRetry     = retryRequest(response)
        // if(isRetry) $.ajax(this)
      },
      success: function(res) {
        const response  = res.data
        const totalPage = Math.ceil(res.recordsTotal/10)

        renderVideo(response)
        renderVideoPagination(totalPage)
      }
    });
  }

  function renderVideo(video){
    let videoHtml = ``
    for(const item of video){
      let title = item.title.length > 30 ? item.title.slice(0, 30) + "..." : item.title
      const link = `${WEB_URL}galeri/video/${item.alias}`
      const itemHtml = `<div class="col-lg-6 col-md-12 col-sm-6 col-12 my-3">
        <div class="card">
          <a href="${link}">
            <img src="https://img.youtube.com/vi/${item.video_url}/0.jpg" class="card-img-top" alt="${item.title}">
          </a>
          <div class="card-body">
            <p class="card-text mb-1">
              <a href="${link}">${title}</a>
            </p>
            <span class="card-subtitle mb-2 text-muted" style="font-size: 12px;">${format_date_ID(item.created_at)}</span>
          </div>
        </div>
      </div>`
      videoHtml += itemHtml
    }

    if(videoHtml == ''){
      videoHtml = `<div class="col-12 d-flex justify-content-center"><i>Belum Ada Video</i></div>`
    }

    $('#list-video').html(videoHtml)
  }

function renderVideoPagination(totalPage){
    if(totalPage > 1){
        let fullPagesHtml = ``
        let previousDisable = PAGE_NUMBER == 1 ? 'disabled' : ''
        let nextDisable = PAGE_NUMBER == totalPage ? 'disabled' : ''
        let isPageLimit = totalPage > 5 ? true : false;

        let previousHtml = `<li class="page-item ${previousDisable}">
            <a class="page-link video-pagination-previous" href="#">
                <i class="fas fa-chevron-left"></i>
            </a>
        </li>`
        let nextHtml = `<li class="page-item ${nextDisable}">
            <a class="page-link video-pagination-next" href="#">
                <i class="fas fa-chevron-right"></i>
            </a>
        </li>`
        
        let pagesHtml = ``
        for(let i=1; i<=totalPage; i++){
            const pageActive = i == PAGE_NUMBER ? 'active' : ''
            const pageDisabled = i == PAGE_NUMBER ? '' : 'href="#"'
            let pageHtml = ``
            if(isPageLimit){
                if(i == PAGE_NUMBER){
                    pageHtml = `<li class="page-item ${pageActive} ${pageDisabled}">
                        <a class="page-link video-pagination-item" data-page="${i}" ${pageDisabled}>${i}</a>
                    </li>`
                }else if(i == PAGE_NUMBER-1 || i == PAGE_NUMBER+1){
                    pageHtml = `<li class="page-item ${pageActive} ${pageDisabled}">
                        <a class="page-link video-pagination-item" data-page="${i}" ${pageDisabled}>${i}</a>
                    </li>`
                }else if(i == PAGE_NUMBER-10 || i == PAGE_NUMBER+10){
                    pageHtml = `<li class="page-item disabled">
                        <span class="page-link video-pagination-item">...</span>
                    </li>`
                }
            }else{
                pageHtml = `<li class="page-item ${pageActive} ${pageDisabled}">
                    <a class="page-link video-pagination-item" data-page="${i}" ${pageDisabled}>${i}</a>
                </li>`
            }
            pagesHtml += pageHtml
        }
        fullPagesHtml = `${previousHtml}${pagesHtml}${nextHtml}`
        $(`#video-pagination`).html(fullPagesHtml)
    }else{
        $(`#video-pagination`).html("")
    }
    
}
