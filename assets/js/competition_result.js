$(document).ready(function(){
  get_cabang()

  let result_datatable = $('#result-datatable').DataTable({
    lengthChange: false, // Menonaktifkan opsi "Show Entries"
    info: false, // Menonaktifkan keterangan "Show Entries"
    language: {
      lengthMenu: "_MENU_ data per halaman",
      zeroRecords: "Data tidak ditemukan",
      search: "",
      searchPlaceholder: "Cari..",
      info: "Halaman _PAGE_ dari _PAGES_",
      infoEmpty: "Belum ada data",
      infoFiltered: "(filtered from _MAX_ total records)",
      loadingRecords: "Memuat...",
      processing: "Memuat",
      paginate: {
        next: "Selanjutnya",
        previous: "Sebelumnya"
      }
    }
  })

  if (result_datatable.page.info().pages === 1) {
    $('.dataTables_paginate').hide();
  }

  $('#babak-option').change(function(){
    $('#cabang-option').val("").change()
  })

  $('#cabang-option').change(function(){
    let value                 = $(this).val()
    let is_golongan_disabled  = value ? false : true

    $('#golongan-option').prop("disabled", is_golongan_disabled)
    $('#golongan-option').val("").change()
    if(!is_golongan_disabled){
      get_golongan_by_cabang(value)
    }
    
  })

  $('#golongan-option').change(function(){
    let value                 = $(this).val()
    let result_section_display= value ? 'block' : 'none'
    let init_section_display  = value ? 'none' : 'block'
    $('#result-section').css("display", result_section_display)
    $('#init-section').css("display", init_section_display)
  })
})

function get_cabang(){
	$.ajax({
		async: true,
		url: `${SCORE_API_URL}cabang`,
		type: 'GET',
		error: function(res) {
			const response  = JSON.parse(res.responseText)
			// render_not_found()
		},
		success: function(res) {
			const response  = res.data
			console.log(response)
      render_cabang(response)
		}
	});
}

function get_golongan_by_cabang(cabang_id){
	$.ajax({
		async: true,
		url: `${SCORE_API_URL}golongan-by-cabang/${cabang_id}`,
		type: 'GET',
		error: function(res) {
			const response  = JSON.parse(res.responseText)
			// render_not_found()
		},
		success: function(res) {
			const response  = res.data
			console.log(response)
      render_golongan(response)
		}
	});
}

function render_cabang(data){
  let cabang_html = '<option value="">--Pilih cabang--</option>'
  data.forEach(item => {
    let item_html = `<option value="${item.cabang_id}">${item.cabang_nama}</option>`
    cabang_html += item_html
  });

  $('#cabang-option').html(cabang_html)
}

function render_golongan(data){
  let golongan_html = '<option value="">--Pilih golongan--</option>'
  data.forEach(item => {
    let item_html = `<option value="${item.gol_id}">${item.gol_nama}</option>`
    golongan_html += item_html
  });

  $('#golongan-option').html(golongan_html)
}

function render_profile_detail(profile){
  //image
  $('#profile-date').html(format_date_ID(profile.created_at))
  $('#profile-title').html(profile.title)
  $('#profile-content').html(profile.content)
}

function render_not_found(){
  $('#profile-nav-section').css('display', 'none')
  $('#profile-title').css('display', 'none')
  $('#profile-content').html('<p align="center"><i>Halaman tidak ditemukan</i></p>')
}