let TITLE = window.location.pathname.split("/").pop()

$(document).ready(function(){
  get_video_detail()
});

function get_video_detail(){
  $.ajax({
    async: false,
    url: `${VIDEO_API_URL}by-alias/${TITLE}`,
    type: 'GET',
    error: function(res) {
      const response  = JSON.parse(res.responseText)
      // let isRetry     = retryRequest(response)
      // if(isRetry) $.ajax(this)
      render_not_found()
    },
    success: function(res) {
      const response  = res.data

      render_video_detail(response)
    }
  });
}

function render_video_detail(video){
  //image
  $('#video-player').prop("src", `https://www.youtube.com/embed/${video.video_url}`)
  $('#video-player').prop("title", video.title)
  $('#video-date').html(format_date_ID(video.created_at))
  $('#video-title').html(video.title)
  $('#video-description').html("Deskripsi: "+video.description)
}

function render_not_found(){
  $('#video-nav-section').css('display', 'none')
  $('#video-title').css('display', 'none')
  $('#video-content').css('display', 'none')
  $('#video-description').html('<p align="center"><i>Halaman tidak ditemukan</i></p>')
}

