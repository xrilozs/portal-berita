//DUMMY DATA
let SCHEDULES = []

$(document).ready(function(){

	get_active_event()

	$('#close-pdf').click(function() {
    $('html, body').animate({ scrollTop: 0 }, 'fast');
    return false;
  });

});

function get_active_event(){
	$.ajax({
		async: true,
		url: `${SCHEDULE_API_URL}active`,
		type: 'GET',
		error: function(res) {
			const response  = JSON.parse(res.responseText)
			render_not_found()
		},
		success: function(res) {
			const response  = res.data
			console.log(response)
			$('#event-name').html(response.event_nama)
			$("#pdf").attr("src", response.full_pdf_url)
			let items = response.items.reverse()
			SCHEDULES = items
			render_timeline(items)
			render_rundown(items[0])
		}
	});
}

function render_not_found(){
	$('#schedule-pdf-section').hide()
	$('#schedule-detail').hide()
  $('#schedule-timeline').html(`<p align="center">
		<i>Belum ada Jadwal Perlombaan</i>
	</p>`)
}

function render_timeline(items){
	let timeline_html = ""
	items.forEach((item, index) => {
		let date_arr = item.date.split("-")
		let item_html = `<div class="timeline-item" data-slick-index="${index}">
			<div class="card bg-success text-white text-center">
				<div class="card-body">
					<h1 class="fw-bold">${date_arr[2]}</h1>
					<span>${date_arr[1]}</span><br>
					<span>${date_arr[0]}</span>
				</div>
			</div>
		</div>`
		timeline_html += item_html
	});

	$('#schedule-timeline').html(timeline_html)
	render_timeline_slick()
}

function render_rundown(data){
	let rundown = JSON.parse(data.schedule_json)
	let rundown_content_html = ""
	rundown.forEach(item => {
		let item_html = `<li class="event" data-date="${item.hour}">
				<h3>${item.title}</h3>
				<p>${item.description}</p>
		</li>`
		rundown_content_html += item_html
	});

	$('#rundown-title').html(`Jadwal ${format_date_ID(data.date)}`)
	$('#rundown-content').html(rundown_content_html)
}

function render_timeline_slick(){
	$(".timeline-center").slick({
		dots: false,
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: true,
					dots: true,
				},
			},
			{
				breakpoint: 576,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: true,
					dots: true,
				},
			},
		],
	});

	$('.timeline-center').on('click', '.timeline-item', function() {
		var slick = $('.timeline-center').slick('getSlick');
		console.log(slick)
		var clickedIndex = $(this).data('slick-index');
		var currentIndex = slick.currentSlide;
		console.log("click id: ", clickedIndex)
		console.log("current id: ", currentIndex)
		
		// Calculate the difference between clicked item index and current slide index
		var indexDiff = clickedIndex - currentIndex;
		
		// If the clicked item is not in the center, navigate to it
		if (indexDiff !== 0) {
			$('.timeline-center').slick('slickGoTo', currentIndex + indexDiff);
			render_rundown(SCHEDULES[clickedIndex])
		}
		
		// Perform further actions based on the selected item
		var selectedItem = $(this).text();
		console.log('Selected item:', selectedItem);
	});
}