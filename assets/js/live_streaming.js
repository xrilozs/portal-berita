$(document).ready(function(){

  get_live_streaming()
});

function get_live_streaming(){
	$.ajax({
		async: true,
		url: `${LIVE_STREAMING_API_URL}all`,
		type: 'GET',
		error: function(res) {
			const response  = JSON.parse(res.responseText)
			// let isRetry     = retryRequest(response)
			// if(isRetry) $.ajax(this)
		},
		success: function(res) {
			const response  = res.data

			if(response.length > 0){
				let first_video = response[0]
				render_live_streaming(first_video)
				render_live_streaming_list(response)
			}else{
				render_not_found()
			}
		}
	});
}

function render_live_streaming(live_streaming){
	$('#live-streaming-player').prop("src", `https://www.youtube.com/embed/${live_streaming.video_url}`)
  $('#live-streaming-player').prop("title", live_streaming.title)
}

function render_live_streaming_list(live_streamings){
	let live_streaming_html = ""
	live_streamings.forEach((item, index) => {
		let item_html = `<div class="timeline-item" data-slick-index="${index}" data-url="${item.video_url}" data-title="${item.title}">
      <div class="card">
        <img src="https://img.youtube.com/vi/${item.video_url}/maxresdefault.jpg" class="card-img-top" alt="${item.title}">
        <div class="card-body" style="height: 100px;">
          <p class="card-text mb-1">
            <span>${item.title}</span>
          </p>
          <span class="card-subtitle mb-2 text-muted" style="font-size: 12px;">${format_date_ID(item.created_at)}</span>
        </div>
      </div>
    </div>`
		live_streaming_html += item_html
	});

	$('.timeline-center').html(live_streaming_html)
	render_slick()
}

function render_not_found(){
	$('#live-streamin-focus').hide()
	$('#live-streaming-nav-section').hide()
  $('.timeline-center').html(`<p align="center">
		<i>Belum ada live streaming</i>
	</p>`)
}

function render_slick(){
	$(".timeline-center").slick({
		dots: false,
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: true,
					dots: true,
				},
			},
			{
				breakpoint: 576,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: true,
					dots: true,
				},
			},
		],
	});

	$('.timeline-center').on('click', '.timeline-item', function() {
		var slick = $('.timeline-center').slick('getSlick');
		console.log(slick)
		var video_url 	 = $(this).data('url');
		var video_title  = $(this).data('title');
		let live_streaming = {
			video_url: video_url,
			title: video_title
		}
		var clickedIndex = $(this).data('slick-index');
		var currentIndex = slick.currentSlide;
		console.log("click id: ", clickedIndex)
		console.log("current id: ", currentIndex)
		
		// Calculate the difference between clicked item index and current slide index
		var indexDiff = clickedIndex - currentIndex;
		
		// If the clicked item is not in the center, navigate to it
		if (indexDiff !== 0) {
			$('.timeline-center').slick('slickGoTo', currentIndex + indexDiff);
			console.log("live streaming", live_streaming)
			render_live_streaming(live_streaming)
		}
		
		// Perform further actions based on the selected item
		var selectedItem = $(this).text();
		console.log('Selected item:', selectedItem);
	});
}