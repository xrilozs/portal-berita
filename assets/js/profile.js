let TITLE = window.location.pathname.split("/").pop()

$(document).ready(function(){
  get_profile_detail()
})

function get_profile_detail(){
  $.ajax({
    async: true,
    url: `${PROFILE_API_URL}by-alias/${TITLE}`,
    type: 'GET',
    error: function(res) {
      const response  = JSON.parse(res.responseText)
      // let isRetry     = retryRequest(response)
      // if(isRetry) $.ajax(this)
      render_not_found()
    },
    success: function(res) {
      const response  = res.data

      render_profile_detail(response)
    }
  });
}

function render_profile_detail(profile){
  //image
  $('#profile-date').html(format_date_ID(profile.created_at))
  $('#profile-title').html(profile.title)
  $('#profile-content').html(profile.content)
}

function render_not_found(){
  $('#profile-nav-section').css('display', 'none')
  $('#profile-title').css('display', 'none')
  $('#profile-content').html('<p align="center"><i>Halaman tidak ditemukan</i></p>')
}