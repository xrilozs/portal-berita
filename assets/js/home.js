$(document).ready(function(){
    get_slider_news()
    get_home_news()
})

function get_home_news(){
  $.ajax({
    async: true,
    url: `${NEWS_API_URL}?page_number=0&page_size=10&status=PUBLISH`,
    type: 'GET',
    error: function(res) {
      const response = JSON.parse(res.responseText)
      // let isRetry = retryRequest(response)
      // if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const response = res.data
      renderHomeNews(response)
    }
  });
}

function renderHomeNews(news){
  let newsHtml = ``
  for(const item of news){
    let title = item.title.length > 30 ? item.title.slice(0, 30) + "..." : item.title
    let category = item.category_news_name.toLowerCase()
    const link = `${WEB_URL}berita/${category}/${item.alias}`
    const itemHtml = `<div class="col-lg-6 col-md-12 col-sm-6 col-12 my-3">
      <div class="row">
        <div class="col-xl-4 col-lg-5 col-md-4 col-sm-5 col-4">
          <img src="${item.full_img_url}" style="width:100px;height:100%; object-fit: cover"><br>
        </div>
        <div class="col-xl-8 col-lg-7 col-md-8 col-sm-7 col-8">
          <div class="row">
              <div class="col-12">
              <p>
                  <a href="${link}">${title}</a>
              </p>
              </div>
              <div class="col-12">
                <span class="text-success" style="font-size:12px;">${item.created_at}</span>
              </div>
          </div>
        </div>
      </div>
    </div>`
    newsHtml += itemHtml
  }

  if(newsHtml == ''){
    newsHtml = `<div class="col-12 d-flex justify-content-center"><i>Belum Ada Berita</i></div>`
  }

  $('#home-news').html(newsHtml)
}
